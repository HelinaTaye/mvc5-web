﻿using Todo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Todo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Tasks()
        {
            IList<Tasks> ta = new List<Tasks>();
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            var query = from note in db2.Tasks select note;
            var tasks = query.ToList();
            foreach (var t in tasks)
            {
                ta.Add(new Tasks() { id = t.Id, task =t.task1,due=t.due });
            }

            


            return View(ta);
        }

        public ActionResult Search(int id)
        {
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            Tasks t = db2.Tasks.Where(x => x.Id == id).Select(x =>
                                                new Tasks()
                                                {
                                                    id = x.Id,
                                                    task=x.task1,
                                                    due=x.due

                                                }).SingleOrDefault();

            return View(t);
        }
        public ActionResult Update(int id) 
        {
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            Tasks t = db2.Tasks.Where(x => x.Id == id).Select(x =>
                                                new Tasks()
                                                {
                                                    id = x.Id,
                                                    task = x.task1,
                                                    due = x.due

                                                }).SingleOrDefault();

            return View(t);
        }
        [HttpPost]
        public ActionResult Add(Tasks tasks)
        {
            
            using (DataClasses1DataContext db2 = new DataClasses1DataContext())
            {
                Task t = new Task();
                t.task1 = tasks.task;
                t.due = tasks.due;




                db2.Tasks.InsertOnSubmit(t);
                db2.SubmitChanges();


            }
            return RedirectToAction("Index");


        }


        [HttpPost]
        [ActionName("Update")]
        public ActionResult EditP(Tasks tasks)
        {
            try
            {
                DataClasses1DataContext db2 = new DataClasses1DataContext();
                Task t = db2.Tasks.Where(x => x.Id == tasks.id).Single<Task>();

                t.task1 = tasks.task;
                t.due = tasks.due;
                db2.SubmitChanges();
                return RedirectToAction("Tasks");
            }
            catch
            {
                return View(tasks);
            }
        }
        public ActionResult Remove(Tasks tasks)
        {
            try
            {
                DataClasses1DataContext db2 = new DataClasses1DataContext();
                Task t = db2.Tasks.Where(x => x.Id == tasks.id).Single<Task>();
                db2.Tasks.DeleteOnSubmit(t);
                db2.SubmitChanges();
                return RedirectToAction("Tasks");
            }
            catch
            {
                return View(tasks);
            }

        }

    }
}